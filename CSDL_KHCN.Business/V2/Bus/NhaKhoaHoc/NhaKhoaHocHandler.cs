﻿using LinqKit;
using CSDL_KHCN.Data_V2;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace CSDL_KHCN.Business_V2
{
    public class NhaKhoaHocHandler : INhaKhoaHocHandler
    {
        private readonly DbHandler<BusNhaKhoaHoc, NhaKhoaHocModel, NhaKhoaHocQueryModel> _dbHandler = DbHandler<BusNhaKhoaHoc, NhaKhoaHocModel, NhaKhoaHocQueryModel>.Instance;

        #region CRUD
        public Task<Response> FindAsync(Guid id)
        {
            return _dbHandler.FindAsync(id);
        }
        public async Task<Response> CreateAsync(NhaKhoaHocCreateModel model, Guid applicationId, Guid userId)
        {

            BusNhaKhoaHoc request = AutoMapperUtils.AutoMap<NhaKhoaHocCreateModel, BusNhaKhoaHoc>(model);
            request = request.InitCreate(applicationId, userId);
            request.Id = Guid.NewGuid();
            var result = await _dbHandler.CreateAsync(request);
            return result;
        }
        public async Task<Response> UpdateAsync(Guid id, NhaKhoaHocUpdateModel model, Guid applicationId, Guid userId)
        {
            BusNhaKhoaHoc request = AutoMapperUtils.AutoMap<NhaKhoaHocUpdateModel, BusNhaKhoaHoc>(model);
            request = request.InitUpdate(applicationId, userId);
            request.Id = id;
            var result = await _dbHandler.UpdateAsync(id, request);
            return result;
        }
        public async Task<Response> DeleteAsync(Guid id)
        {
            var result = await _dbHandler.DeleteAsync(id);
            return result;
        }
        public async Task<Response> DeleteRangeAsync(IList<Guid> listId)
        {
            var result = await _dbHandler.DeleteRangeAsync(listId, "Id");
            return result;
        }
        #endregion
        public Response GetAll()
        {
            return _dbHandler.GetAll();
        }
        public async Task<Response> GetAllAsync()
        {
            var predicate = PredicateBuilder.New<BusNhaKhoaHoc>(true);
            return await _dbHandler.GetAllAsync(predicate);
        }
        public Task<Response> GetAllAsync(NhaKhoaHocQueryModel query)
        {
            var predicate = BuildQuery(query);
            return _dbHandler.GetAllAsync(predicate);
        }
        public Task<Response> GetPageAsync(NhaKhoaHocQueryModel query)
        {
            var predicate = BuildQuery(query);
            return _dbHandler.GetPageAsync(predicate, query);
        }
        private Expression<Func<BusNhaKhoaHoc, bool>> BuildQuery(NhaKhoaHocQueryModel query)
        {
            var predicate = PredicateBuilder.New<BusNhaKhoaHoc>(true);

            if (!string.IsNullOrEmpty(query.LinhVuc))
            {
                predicate.And(s => s.LinhVuc.Contains(query.LinhVuc));
            }
            if (!string.IsNullOrEmpty(query.ChucDanhNghienCuu))
            {
                predicate.And(s => s.ChucDanhNghienCuu == query.ChucDanhNghienCuu);
            }

            if (!string.IsNullOrEmpty(query.HoTen))
            {
                predicate.And(s => s.HoTen == query.HoTen);
            }
            if (query.ListId != null && query.ListId.Count > 0)
            {
                predicate.And(s => query.ListId.Contains(s.Id));
            }
            if (!string.IsNullOrEmpty(query.FullTextSearch))
            {
                predicate.And(s => s.HoTen.Contains(query.FullTextSearch) || s.HoTen.Contains(query.FullTextSearch));
            }
            return predicate;
        }
    }
}
