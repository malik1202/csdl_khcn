﻿using CSDL_KHCN.Data_V2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace CSDL_KHCN.Business_V2
{
    public class RoleCollection
    {
        private readonly IRoleHandler _handler;
        public HashSet<RoleModel> Collection;
        public static RoleCollection Instance { get; } = new RoleCollection();

        protected RoleCollection()
        {
            _handler = new RoleHandler();
            LoadToHashSet();
        }

        public void LoadToHashSet()
        {
            Collection = new HashSet<RoleModel>();
            // Query to list
            var listResponse = _handler.GetAll();
            // Add to hashset
            if (listResponse.Code == Code.Success)
            {
                // Add to hashset
                if (listResponse is ResponseObject<List<RoleModel>> listResponseData)
                    foreach (var response in listResponseData.Data)
                    {
                        Collection.Add(response);
                    }
            }
        }
        public string GetName(Guid id)
        {
            var result = Collection.FirstOrDefault(u => u.Id == id);
            return result?.Name;
        }
        public BaseRoleModel GetModel(Guid id)
        {
            var result = Collection.FirstOrDefault(u => u.Id == id);
            return result;
        }
        public BaseRoleModel GetModel(string userName)
        {
            var result = Collection.FirstOrDefault(u => u.Name == userName);
            return result;
        }

        public List<BaseRoleModel> GetModel(Expression<Func<BaseRoleModel, bool>> predicate)
        {
            var result = Collection.AsQueryable().Where(predicate);
            return result.ToList();
        }
    }
}