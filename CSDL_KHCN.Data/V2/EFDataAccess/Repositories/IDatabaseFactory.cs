using Microsoft.EntityFrameworkCore;

namespace CSDL_KHCN.Data_V2
{
    public interface IDatabaseFactory
    {
        DbContext GetDbContext();
        string GetPrefix();
    }
}