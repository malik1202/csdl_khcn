﻿using CSDL_KHCN.Business_V2;
using CSDL_KHCN.Data_V2;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CSDL_KHCN.API_V2
{
    /// <inheritdoc />
    /// <summary>
    /// Application controller
    /// </summary>
    [ApiVersion("2.0")]
    [Route("api/v{api-version:apiVersion}/sys/applications")]
    [ApiExplorerSettings(GroupName = "01: System - ApplicationId")]
    public class SysApplicationController : ControllerBase
    {
        private readonly IApplicationHandler _applicationHandler;

        public SysApplicationController(IApplicationHandler applicationHandler)
        {
            _applicationHandler = applicationHandler;
        }

        #region CRUD

        /// <summary>
        /// Thêm mới
        /// </summary>
        /// <param name="model">Dữ liệu</param>
        /// <returns>Kết quả trả về</returns>
        /// <response code="200">Thành công</response>
        [Authorize, HttpPost, Route("")]
        [ProducesResponseType(typeof(ResponseObject<ApplicationModel>), StatusCodes.Status200OK)]
        //[SwaggerRequestExample(typeof(ApplicationCreateModel), typeof(MockupObject<ApplicationCreateModel>))]
        public async Task<IActionResult> CreateAsync([FromBody] ApplicationCreateModel model)
        {
            // Call service
            var result = await _applicationHandler.CreateAsync(model);
            // Hander response
            return Helper.TransformData(result);
        }

        /// <summary>
        /// Cập nhật
        /// </summary>
        /// <param name="id">Id bản ghi</param>
        /// <param name="model">Dữ liệu</param>
        /// <returns>Kết quả trả về</returns>
        /// <response code="200">Thành công</response>
        [Authorize, HttpPut, Route("{id}")]
        [ProducesResponseType(typeof(ResponseUpdate), StatusCodes.Status200OK)]
        //[SwaggerRequestExample(typeof(ApplicationUpdateModel), typeof(MockupObject<ApplicationUpdateModel>))]
        public async Task<IActionResult> UpdateAsync(Guid id, [FromBody] ApplicationUpdateModel model)
        {
            // Call service
            var result = await _applicationHandler.UpdateAsync(id, model);

            // Hander response
            return Helper.TransformData(result);
        }

        /// <summary>
        /// Xóa
        /// </summary>
        /// <param name="id">Id bản ghi</param>
        /// <returns>Kết quả trả về</returns>
        /// <response code="200">Thành công</response>
        [Authorize, HttpDelete, Route("{id}")]
        [ProducesResponseType(typeof(ResponseDelete), StatusCodes.Status200OK)]
        public async Task<IActionResult> DeleteAsync(Guid id)
        {
            // Call service
            var result = await _applicationHandler.DeleteAsync(id);
            // Hander response
            return Helper.TransformData(result);
        }

        /// <summary>
        /// Xóa danh sách
        /// </summary>
        /// <param name="listId">Danh sách id bản ghi</param>
        /// <returns>Kết quả trả về</returns>
        /// <response code="200">Thành công</response>
        [Authorize, HttpDelete, Route("")]
        [ProducesResponseType(typeof(ResponseDeleteMulti), StatusCodes.Status200OK)]
        public async Task<IActionResult> DeleteRangeAsync([FromQuery] List<Guid> listId)
        {
            // Call service 
            var result = await _applicationHandler.DeleteRangeAsync(listId);
            // Hander response
            return Helper.TransformData(result);
        }

        /// <summary>
        /// Lấy về theo Id
        /// </summary>
        /// <param name="id">Id bản ghi</param>
        /// <returns>Kết quả trả về</returns>
        /// <response code="200">Thành công</response>
        [Authorize, HttpGet, Route("{id}")]
        [ProducesResponseType(typeof(ResponseObject<ApplicationModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetByIdAsync(Guid id)
        {
            // Call service
            var result = await _applicationHandler.FindAsync(id);
            // Hander response
            return Helper.TransformData(result);
        }

        /// <summary>
        /// Lấy về theo bộ loc
        /// </summary>
        /// <param name="page">Số thứ tự trang tìm kiếm</param>
        /// <param name="size">Số bản ghi giới hạn một trang</param>
        /// <param name="filter">Thông tin lọc nâng cao (Object Json)</param>
        /// <param name="sort">Thông tin sắp xếp (Array Json)</param>
        /// <returns></returns>
        /// <remarks>
        ///  *filter*
        ///  ....
        ///  *sort*
        ///  ....
        /// </remarks>
        /// <response code="200">Thành công</response>
        [Authorize, HttpGet, Route("")]
        [ProducesResponseType(typeof(ResponsePagination<ApplicationModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetFilterAsync([FromQuery] int page = 1, [FromQuery] int size = 20,
            [FromQuery] string filter = "{}", [FromQuery] string sort = "")
        {
            // Call service
            var filterObject = JsonConvert.DeserializeObject<ApplicationQueryModel>(filter);
            filterObject.Sort = sort;
            filterObject.Size = size;
            filterObject.Page = page;
            var result = await _applicationHandler.GetPageAsync(filterObject);
            // Hander response
            return Helper.TransformData(result);
        }

        /// <summary>
        /// Lấy về tất cả
        /// </summary> 
        /// <returns></returns> 
        /// <response code="200">Thành công</response>
        [Authorize, HttpGet, Route("all")]
        [ProducesResponseType(typeof(ResponseObject<IList<ApplicationModel>>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetAllAsync()
        {
            // Call service
            var result = await _applicationHandler.GetAllAsync();
            // Hander response
            return Helper.TransformData(result);
        }

        /// <summary>
        /// Lấy về tất cả theo người dùng
        /// </summary> 
        /// <returns></returns> 
        /// <response code="200">Thành công</response>
        [Authorize, HttpGet, Route("owner")]
        [ProducesResponseType(typeof(ResponseObject<IList<ApplicationModel>>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetAllOwnerAsync()
        {
            // Get Token Info
            var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
            var actorId = requestInfo.UserId;
            var appId = requestInfo.ApplicationId;
            // Call service
            var result = await _applicationHandler.GetAllByUserIdAsync(actorId);
            // Hander response
            return Helper.TransformData(result);
        }

        /// <summary>
        /// Lấy về tất cả theo người dùng
        /// </summary> 
        /// <param name="userId">Id người dùng</param>
        /// <returns></returns> 
        /// <response code="200">Thành công</response>
        [Authorize, HttpGet, Route("user/{userId}")]
        [ProducesResponseType(typeof(ResponseObject<IList<ApplicationModel>>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetAllByUserIdAsync(Guid userId)
        {
            // Call service
            var result = await _applicationHandler.GetAllByUserIdAsync(userId);
            // Hander response
            return Helper.TransformData(result);
        }

        #endregion

        #region OTHER

        /// <summary>
        /// Khởi tạo dữ liệu hệ thống
        /// </summary> 
        /// <returns></returns> 
        /// <response code="200">Thành công</response>
        [AllowAnonymous, HttpPost, Route("bootstrap")]
        [ProducesResponseType(typeof(ResponseObject<IList<int>>), StatusCodes.Status200OK)]
        public async Task<IActionResult> Bootstrap(int option)
        {
            // Call service
            var result = await _applicationHandler.BootstrapProjectDataAsync(option);
            // Hander response
            return Helper.TransformData(result);
        }

        #endregion
    }
}