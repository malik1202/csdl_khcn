// -------------------------------------------------------------------------------------------------
// Copyright (c) Johan Boström. All rights reserved.
// Licensed under the Apache License, Version 2.0. See LICENSE in the project root for license information.
// -------------------------------------------------------------------------------------------------
namespace API.BasicAuth.Middlewares
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Net;
    using System.Net.Http;
    using System.Security.Claims;
    using System.Text;
    using System.Text.Encodings.Web;
    using System.Threading.Tasks;
    using CSDL_KHCN.Business_V2;
    using CSDL_KHCN.Data_V2;
    using Microsoft.AspNetCore.Authentication;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Routing;
    using Microsoft.Extensions.Logging;
    using Microsoft.Extensions.Options;

    public static class CustomAuthExtensions
    {
        public static AuthenticationBuilder AddCustomAuth(this AuthenticationBuilder builder, Action<CustomAuthOptions> configureOptions)
        {
            return builder.AddScheme<CustomAuthOptions, CustomAuthHandler>("Custom Scheme", "Custom Auth", configureOptions);
        }
    }
    public class CustomAuthOptions : AuthenticationSchemeOptions
    {
        public CustomAuthOptions()
        {

        }
    }
    internal class CustomAuthHandler : AuthenticationHandler<CustomAuthOptions>
    {
        private readonly string _serviceValidate = Utils.GetConfig("Authencate:SSO2:Uri");
        private readonly string _clientId = Utils.GetConfig("Authencate:SSO2:Clientid");
        private readonly string _clientSecret = Utils.GetConfig("Authencate:SSO2:Secret");
        private readonly string _redirectUri = Utils.GetConfig("Authencate:SSO2:Redirecturi");
        public CustomAuthHandler(IOptionsMonitor<CustomAuthOptions> options, ILoggerFactory logger, UrlEncoder encoder, ISystemClock clock) : base(options, logger, encoder, clock)
        {
            // store custom services here...
        }
        protected override async Task<AuthenticateResult> HandleAuthenticateAsync()
        {
            await Task.Delay(0);
            string authHeader = Request.Headers["Authorization"];
            AuthenticateResult result = null;
            #region Basic
            if (Utils.GetConfig("Authencate:Basic:Enable") == "true")
            {
                if (authHeader != null && authHeader.StartsWith("Basic "))
                {
                    // Get the encoded username and password
                    var encodedUsernamePassword = authHeader.Split(' ', 2, StringSplitOptions.RemoveEmptyEntries)[1]?.Trim();
                    // Decode from Base64 to string
                    var decodedUsernamePassword = Encoding.UTF8.GetString(Convert.FromBase64String(encodedUsernamePassword));
                    // Split username and password
                    var username = decodedUsernamePassword.Split(':', 2)[0];
                    var password = decodedUsernamePassword.Split(':', 2)[1];
                    // Check if login is correct
                    if (username == Utils.GetConfig("Authencate:AdminUser") && username == Utils.GetConfig("Authencate:AdminPassWord"))
                    {

                        var claims = new List<Claim>();
                        claims.Add(new Claim(ClaimTypes.NameIdentifier, UserConstants.AdministratorId.ToString()));
                        ClaimsIdentity claimsIdentity = new ClaimsIdentity(claims, "Basic");
                        ClaimsPrincipal claimsPrincipal = new ClaimsPrincipal(claimsIdentity);

                        result = AuthenticateResult.Success(new AuthenticationTicket(claimsPrincipal,
                                new AuthenticationProperties(), "Basic"));
                        return result;
                    }
                }
                return AuthenticateResult.NoResult();
            }
            #endregion
            else
            {
                var claims = new List<Claim>();
                claims.Add(new Claim(ClaimTypes.NameIdentifier, UserConstants.AdministratorId.ToString()));
                ClaimsIdentity claimsIdentity = new ClaimsIdentity(claims, "NONE_AUTH");
                ClaimsPrincipal claimsPrincipal = new ClaimsPrincipal(claimsIdentity);

                result = AuthenticateResult.Success(new AuthenticationTicket(claimsPrincipal,
                        new AuthenticationProperties(), "NONE_AUTH"));
                return result;
            }

        }
    }
}