﻿namespace CSDL_KHCN.API.Constants
{
    public static class CorsPolicyName
    {
        public const string AllowAny = nameof(AllowAny);
        public const string AllowFrontEnd = nameof(AllowFrontEnd);
        public const string AllowThirdparty = nameof(AllowThirdparty);
    }
}
